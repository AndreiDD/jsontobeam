package main

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

// TestSimpleLoading tests correct loading of a test-data file
func TestSimpleLoading(t *testing.T) {
	// Test for no arguments
	file, err := os.Open("./sample-data/test-data.json")
	if err != nil {
		t.Error(err)
	}
	col, err := loadCollection(file)
	assert.Equal(t, len(col), 38, "test data should contain 38 rows")
}
