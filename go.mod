module gokatax

go 1.13

require (
	github.com/apache/beam v2.18.0+incompatible
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/googleapis/gax-go v2.0.2+incompatible // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0
	github.com/zerjioang/go-bus v0.2.3
	go.opencensus.io v0.22.2 // indirect
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa // indirect
	golang.org/x/sys v0.0.0-20191228213918-04cbcbbfeed8 // indirect
	google.golang.org/api v0.15.0 // indirect
	google.golang.org/genproto v0.0.0-20191230161307-f3c370f40bfb // indirect
	google.golang.org/grpc v1.26.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
