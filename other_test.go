package main

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

// TestLineCounting tests if it correctly counts how many lines a txt file has
func TestLineCounting(t *testing.T) {
	// Test for no arguments
	file, err := os.Open("./sample-data/test-data.json")
	if err != nil {
		t.Error(err)
	}
	lines, err := LineCounter(file)
	assert.Equal(t, lines, 38, "test data should contain 38 rows")
}
