package main

import (
	"bytes"
	"github.com/sirupsen/logrus"
	"io"
	"log"
	"os"
)

// Close - check for error on an io.Closer
func Close(c io.Closer) {
	err := c.Close()
	if err != nil {
		logrus.Error(err.Error())
	}
}

// LineCounter counts the lines in a file
func LineCounter(r io.Reader) (int, error) {
	buf := make([]byte, 32*1024)
	count := 0
	lineSep := []byte{'\n'}

	for {
		c, err := r.Read(buf)
		count += bytes.Count(buf[:c], lineSep)

		switch {
		case err == io.EOF:
			return count, nil

		case err != nil:
			return count, err
		}
	}
}

// DeleteFile .
func DeleteFile(path string) {
	var err = os.Remove(path)
	if err != nil {
		log.Fatal(err)
	}
}
