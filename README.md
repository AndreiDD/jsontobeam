# apache beam go

Go is not the preferred language to do this, since Beam doesn't support it so good. 
If I would have to do this exercise again, I would go for Java or Python... but I realised this after I finished it

## How to run it

install go (v 1.11+)

Presuming the user pseudo id is a unique identifier for each user...

on linux:
~~~~
go build -o app
./app
~~~~

test

~~~~
go test -v -race -cover ./...
~~~~

Results: 


- on 20191230 = 31 active users
- on 20191231 = 33 active users


----------------------------------------

sample-data/test-data.json  -> consists of 3 engagements, with 1 being less than 3 seconds => used for tests. 

-----------------------------------------

### Things to consider


If a phone's battery die, or no network, the data will arrive at a later point. then in apache beam you have
watermark to keep the window open during some more time than initially defined in the window.

Beam: You need Java or Python... Go support is not as great as Java's one, (or even Python's) and doesn't contain many things
also if you want to use Custom window patters you're stuck only with Java


### Scaling:


low cost:

Kafka Cluster -> Apache Beam (low latency and mini batch) -> Apache Samza
hosted on own dedicated servers


high cost:
If you're a fan of giving Google your $$$ then

streaming Taxi in Pub/Sub -> Beam -> stream & monitor pipeline in BigQuery -> Data Studio...


Example... launch dataflow with parameters

~~~~
— numWorkers=?
— workerMachineType=n1-standard-8 (should do it)
— autoscalingAlgorithm=THROUGHPUT_BASED
— maxNumWorkers=50   <---  
~~~~

disk_size_gb=20 (? or lower)

Dataflow provides an autoscaling feature, which means we can absorb peaks of traffic as more machines 
will be added to unburden current workers if needed.

Things to consider:

Streaming on the cloud is VERY expensive. If possible a solution should be investigated where you change the streaming to File Loads


