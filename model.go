package main

// Entry is our model
type Entry struct {
	EventDate   string `json:"event_date"`
	EventName   string `json:"event_name"`
	EventParams []struct {
		Key   string `json:"key"`
		Value struct {
			IntValue string `json:"int_value"`
		} `json:"value"`
	} `json:"event_params"`
	EventServerTimestampOffset string `json:"event_server_timestamp_offset"`
	EventTimestamp             string `json:"event_timestamp"`
	Language                   string `json:"language,omitempty"`
	MobileOsHardwareModel      string `json:"mobile_os_hardware_model,omitempty"`
	TimeZoneOffsetSeconds      string `json:"time_zone_offset_seconds,omitempty"`
	UserFirstTouchTimestamp    string `json:"user_first_touch_timestamp,omitempty"`
	UserProperties             []struct {
		Key   string `json:"key"`
		Value struct {
			SetTimestampMicros string `json:"set_timestamp_micros,omitempty"`
			StringValue        string `json:"string_value,omitempty"`
		} `json:"value,omitempty"`
	} `json:"user_properties,omitempty"`
	UserPseudoID string `json:"user_pseudo_id,omitempty"`
	Version      string `json:"version,omitempty"`
}
