package main

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"github.com/apache/beam/sdks/go/pkg/beam"
	_ "github.com/apache/beam/sdks/go/pkg/beam/io/filesystem/local"
	"github.com/apache/beam/sdks/go/pkg/beam/io/textio"
	"github.com/apache/beam/sdks/go/pkg/beam/log"
	"github.com/apache/beam/sdks/go/pkg/beam/transforms/filter"
	"github.com/apache/beam/sdks/go/pkg/beam/x/beamx"
	"io"
	"os"
	"strconv"
)

// filter by date
type filterByDate struct {
	Date string `json:"event_date"`
}

// filter by event name: ex: user_engagement
type filterByEventName struct {
	EventName string `json:"event_name"`
}

// filter by an engagement time
type filterByEngagementTime struct {
	EngagementTime float64 `json:"engagement_time_msec"`
}

// helper function to extract an engagement time. replace it with json bugger maybe ?
func extractEngagementTimeFn(row Entry) float64 {
	var engTime int
	for _, ep := range row.EventParams {
		if ep.Key == "engagement_time_msec" {
			tA, err := strconv.Atoi(ep.Value.IntValue)
			if err != nil {
				log.Errorf(context.Background(), "engagement_time_msec did not return an int value %s",
					err.Error())
			}
			engTime = tA
		}
	}
	return float64(engTime)
}

// should match the date
func (f *filterByDate) ProcessElement(row Entry, emit func(Entry)) {
	if row.EventDate == f.Date {
		emit(row)
	}
}

// should match the event name
func (f *filterByEventName) ProcessElement(row Entry, emit func(Entry)) {
	if row.EventName == f.EventName {
		emit(row)
	}
}

// should be greater or equal to the specified time
func (f *filterByEngagementTime) ProcessElement(row Entry, emit func(Entry)) {
	engTime := extractEngagementTimeFn(row)
	if engTime >= f.EngagementTime {
		emit(row)
	}
}

// reads an io.Reader and outputs an array of entries. if a row is not correct formatted, it discards it
// and continues from the next one
func loadCollection(in io.Reader) ([]Entry, error) {
	var output []Entry
	scanner := bufio.NewScanner(in)
	for scanner.Scan() {
		var e Entry
		err := json.Unmarshal(scanner.Bytes(), &e)
		if err != nil {
			log.Errorf(context.Background(), "can't unmarshal json %s", err.Error())
			continue
		}
		output = append(output, e)
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return output, nil
}

func main() {
	// hardcoded for now. could be read as an argument
	dateToProcess := "20191231"

	ctx := context.Background()

	//file, err := os.Open("./sample-data/test-data.json")
	file, err := os.Open("./sample-data/bq-results-sample-data.json")
	if err != nil {
		log.Fatal(ctx, err)
	}
	defer Close(file)

	// this is the structure that each line of the text file is marshaled to
	collection, err := loadCollection(file)
	if err != nil {
		log.Fatal(ctx, err)
	}

	// beam.Init() is an initialization hook that must be called on startup.
	beam.Init()

	// Create the Pipeline object and root scope.
	p := beam.NewPipeline()
	s := p.Root()
	entries := beam.CreateList(s, collection)

	//ParDo is a Beam transform for generic parallel processing. The ParDo processing paradigm is similar to
	//the “Map” phase of a Map/Shuffle/Reduce-style algorithm: a ParDo transform considers each element in the input
	//PCollection, performs some processing function (your user code) on that element, and emits zero, one, or
	//multiple elements to an output PCollection.
	filterByDate := beam.ParDo(s, &filterByDate{Date: dateToProcess}, entries)

	// take just events with user_engagement
	filterByEventName := beam.ParDo(s, &filterByEventName{EventName: "user_engagement"}, filterByDate)

	// filter only engagements with more than 3 seconds
	filterByEngagementTime := beam.ParDo(s, &filterByEngagementTime{EngagementTime: 3000}, filterByEventName)

	txtOutput := beam.ParDo(s, func(e Entry) string {
		return e.UserPseudoID
	}, filterByEngagementTime)

	// finally, count only distinct users
	finalOutput := filter.Distinct(s, txtOutput)

	textio.Write(s, "output.txt", finalOutput)

	if err := beamx.Run(ctx, p); err != nil {
		log.Exitf(ctx, "Failed to execute job: %v", err)
	}

	fmt.Println("processing done. add them to the database")

	// this should be changed an
	outputFile, _ := os.Open("output.txt")
	defer Close(outputFile)
	total, err := LineCounter(outputFile)
	if err != nil {
		fmt.Println("we couldn't find any active users in on date")
		os.Exit(1)
	}
	fmt.Printf("for %s we have a total of %d active users", dateToProcess, total)
	DeleteFile("output.txt")
	//INSERT IGNORE INTO active_user_table ( date, active_user_count ) VALUES ( x, y );
}
